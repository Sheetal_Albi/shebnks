import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';

class CustomText extends StatelessWidget {
  final String text;
  final TextAlign textAlign;
  final TextOverflow textOverflow;
  final int maxLines;
  final Color textColor;
  final Color backgroundColor;
  final double fontSize;
  final FontWeight fontWeight;
  final FontStyle fontStyle;
  final double letterSpacing;
  final double wordSpacing;
  final List<Shadow> shadows;
  final TextDecoration textDecoration;
  final String fontFamily;

  CustomText(this.text,
      {this.textAlign,
      this.textOverflow,
      this.maxLines,
      this.textColor = blackColor,
      this.backgroundColor,
      this.fontSize = 14.0,
      this.fontWeight,
      this.fontStyle,
      this.letterSpacing,
      this.wordSpacing,
      this.shadows,
      this.textDecoration,
      this.fontFamily = 'Poppins-Regular'});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: textOverflow,
      maxLines: maxLines,
      style: TextStyle(
        color: textColor,
        backgroundColor: backgroundColor,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle,
        letterSpacing: letterSpacing,
        wordSpacing: wordSpacing,
        shadows: shadows,
        decoration: textDecoration,
        fontFamily: fontFamily,
      ),
    );
  }
}
