import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shebnks/view/constants/Constant.dart';

ThemeData customTheme() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: redColor,
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.light, // status bar icons' color
    systemNavigationBarIconBrightness:
        Brightness.light, //navigation bar icons' color
  ));

  return ThemeData(
    scaffoldBackgroundColor: whiteColor,
    fontFamily: "Muli",
    appBarTheme: appBarTheme(),
    textTheme: textTheme(),
    primaryColor: redColor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}


TextTheme textTheme() {
  return TextTheme(
    bodyText1: TextStyle(color: grayColor),
    bodyText2: TextStyle(color: grayColor),
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    //centerTitle: true,
    color: redColor,
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: whiteColor),
    textTheme: TextTheme(
      headline6: TextStyle(color: whiteColor, fontSize: 18),
    ),
  );
}
