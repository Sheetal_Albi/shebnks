import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';

class CustomTextField extends StatefulWidget {
  final String hint;
  final String label;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final double borderRadius;
  final bool isPassword;
  final TextInputAction action;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final ValueChanged<String> onchange;
  final TextEditingController controller;
  final int minLine;
  final Color hintColor;
  final Color textColor;
  final Color labelColor;

  CustomTextField({
    this.hint,
    this.label,
    this.keyboardType,
    this.icon,
    this.obscureText = false,
    this.borderRadius,
    this.action = TextInputAction.next,
    this.isPassword = false,
    this.validator,
    this.onSaved,
    this.onchange,
    this.controller,
    this.minLine,
    this.hintColor,
    this.textColor,
    this.labelColor,
  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isPassToggle = false;

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return TextFormField(
      obscureText: widget.isPassword ? !isPassToggle : false,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      cursorColor: redColor,
      textInputAction: widget.action,
      minLines: widget.minLine,
      maxLines: widget.minLine,
      style: TextStyle(color: widget.textColor ?? blackColor),
      decoration: InputDecoration(
        prefixIcon: (widget.icon != null)
            ? Icon(widget.icon, color: redColor, size: 20)
            : null,
        hintText: widget.hint,
        hintStyle:
            TextStyle(color: widget.hintColor ?? blackColor.withOpacity(0.8)),
        labelText: widget.label,
        contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.borderRadius ?? getProportionateScreenHeight(5)),
          borderSide: BorderSide(color: grayColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.borderRadius ?? getProportionateScreenHeight(5)),
          borderSide: BorderSide(color: redColor),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
              widget.borderRadius ?? getProportionateScreenHeight(5)),
          borderSide: BorderSide(color: grayColor),
        ),
        suffixIcon: widget.isPassword
            ? IconButton(
                icon: Icon(
                  isPassToggle ? Icons.visibility_off : Icons.visibility,
                  color: redColor,
                ),
                onPressed: () {
                  setState(() {
                    isPassToggle = !isPassToggle;
                  });
                },
              )
            : null,
      ),
      validator: widget.validator,
      onSaved: widget.onSaved,
      onChanged: widget.onchange,
      /*onEditingComplete: () => node.nextFocus(),*/
    );
  }
}
