import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customText.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    @required this.text,
    @required this.press,
    this.width,
    this.bgColor = redColor,
    this.textColor = whiteColor,
    this.icon,
  }) : super(key: key);

  final String text;
  final Function press;
  final double width;
  final Color bgColor;
  final Color textColor;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Wrap(children: [
        Container(
          width: (width != null) ? width : null,
          height: getProportionateScreenHeight(40),
          decoration: BoxDecoration(
              color: bgColor,
              borderRadius: new BorderRadius.all(
                  Radius.circular(getProportionateScreenHeight(5)))),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenHeight(15)),
            child: Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Visibility(
                    visible: icon != null,
                    child: Icon(
                      icon,
                      color: textColor,
                    ),
                  ),
                  Visibility(
                    visible: icon != null,
                    child: SizedBox(
                      width: getProportionateScreenWidth(10),
                    ),
                  ),
                  CustomText(
                    text,
                    textColor: textColor,
                    fontSize: getProportionateScreenHeight(16),
                  ),
                ],
              ),
            ),
          ),
        )
      ]),
    );
  }
}
