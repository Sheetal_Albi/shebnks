import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shebnks/bloc/LoginBloc.dart';
import 'package:shebnks/model/beans/LoginData.dart';
import 'package:shebnks/model/remote/requests/LoginRequest.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/KeyboardUtil.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/helper/customTextField.dart';
import 'package:shebnks/view/ui/dashboard/dashboardPage.dart';
import 'package:shebnks/view/ui/forgot_password/ForgotPasswordPage.dart';
import 'package:shebnks/view/ui/sign_up/signUpPage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email, password;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(getProportionateScreenHeight(10)),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: getProportionateScreenHeight(80),
                  ),
                  CustomText(
                    appName,
                    fontSize: getProportionateScreenHeight(30),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(30),
                  ),
                  CustomText(
                      "Login with your registered email and password with us to use our services",
                      fontSize: getProportionateScreenHeight(12),
                      textAlign: TextAlign.center),
                  SizedBox(
                    height: getProportionateScreenHeight(50),
                  ),
                  CustomTextField(
                    hint: emailHint,
                    label: emailLabel,
                    keyboardType: TextInputType.emailAddress,
                    onSaved: (newValue) => email = newValue,
                    validator: (value) {
                      if (value.isEmpty) {
                        return emailNull;
                      } else if (!emailValidatorRegExp.hasMatch(value)) {
                        return emailInvalid;
                      }
                      return null;
                    },
                    onchange: (value) => {email = value},
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(15),
                  ),
                  CustomTextField(
                    hint: passwordHint,
                    label: passwordLabel,
                    isPassword: true,
                    minLine: 1,
                    action: TextInputAction.done,
                    onSaved: (newValue) => password = newValue,
                    validator: (value) {
                      if (value.isEmpty) {
                        return passwordNull;
                      } else if (value.length < 8) {
                        return passwordInvalid;
                      }
                      return null;
                    },
                    onchange: (value) => {password = value},
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(15),
                  ),
                  _forgotPassword(),
                  SizedBox(
                    height: getProportionateScreenHeight(25),
                  ),
                  CustomButton(
                      text: login,
                      press: () {
                        _apiLogin();
                      }),
                  SizedBox(
                    height: getProportionateScreenHeight(20),
                  ),
                  _notHaveAccountWidget(),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _forgotPassword() {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ForgotPasswordPage()));
        },
        child: CustomText(
          forgotPassword_,
          fontSize: getProportionateScreenHeight(13),
          textDecoration: TextDecoration.underline,
        ),
      ),
    );
  }

  Widget _notHaveAccountWidget() {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: dontHaveAcc,
            style: TextStyle(
              color: blackColor.withOpacity(0.8),
              fontSize: getProportionateScreenHeight(14),
            ),
          ),
          TextSpan(
            text: registerHere,
            style: TextStyle(
              color: blackColor,
              fontSize: getProportionateScreenHeight(14),
              decoration: TextDecoration.underline,
            ),
            recognizer: new TapGestureRecognizer()
              ..onTap = () => {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => SignUpPage())),
                  },
          ),
        ],
      ),
    );
  }

  Future<void> _apiLogin() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      return;
    }

    if (!await ConstantFunctions.isOnline()) {
      ConstantFunctions.errorToast(noInternet);
      return;
    }

    KeyboardUtil.hideKeyboard(context);
    EasyLoading.show(status: loading);
    LoginData loginVo;
    LoginBloc loginBloc = LoginBloc();
    loginBloc.executeWorkerCategoryData(LoginRequest(email, password));

    loginBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();

        if (value.data.status) {
          loginVo = value.data.result;

          //ConstantFunctions.successToast(value.data.message);

          ConstantFunctions.saveSF(sfUserId, loginVo.user.id);
          ConstantFunctions.saveSF(
              sfUserData, loginVo.user.toJson().toString());
          ConstantFunctions.saveSF(sfToken, value.data.token);

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => DashboardPage(),
            ),
            (route) => false,
          );
        } else {
          print("loginBloc error " + value.data.message);
          ConstantFunctions.errorToast(value.data.message);
        }
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
        print("loginBloc error " + value.error.errorMessage);
        ConstantFunctions.errorToast(value.error.errorMessage);
      }
    });
  }
}
