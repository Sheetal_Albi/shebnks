import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customText.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          bottomProfile,
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10)),
          child: Column(
            children: [
              _profileItem(
                  profileSetting, profileSettingDesc, Icons.settings_outlined),
              _divider(),
              _profileItem(
                  accountManagement, accountManagementDesc, Icons.insert_link),
              _divider(),
              _profileItem(sheLoans, createSeedFundDesc, Icons.add),
              _divider(),
              _profileItem(seedFundApplication, seedFundApplicationDesc,
                  Icons.remove_red_eye_sharp),
              _divider(),
              _profileItem(privacy, privacyDesc, Icons.done),
              _divider(),
              _profileItem(notificationSettings, notificationSettingsDesc,
                  Icons.notification_important_sharp),
              _divider(),
              _profileItem(logout, logoutDesc, Icons.power_settings_new),
            ],
          ),
        ),
      ),
    );
  }

  Widget _profileItem(String title, String desc, IconData icon) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: getProportionateScreenHeight(20)),
      child: Row(
        children: [
          Icon(
            icon,
            size: getProportionateScreenHeight(35),
            color: redColor,
          ),
          SizedBox(
            width: getProportionateScreenWidth(10),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title,
                  fontSize: getProportionateScreenHeight(16),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(3),
                ),
                CustomText(
                  desc,
                  fontSize: getProportionateScreenHeight(12),
                  textColor: grayColor,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _divider() {
    return Container(
      width: double.infinity,
      height: 0.5,
      color: blackColor,
    );
  }
}
