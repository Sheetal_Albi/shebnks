import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/helper/customTextField.dart';

class TransactionsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController searchController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          bottomTransaction,
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(getProportionateScreenHeight(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextField(
              keyboardType: TextInputType.text,
              hint: 'Search',
              icon: Icons.search,
              controller: searchController,
              borderRadius: 5,
              action: TextInputAction.done,
            ),
            SizedBox(
              height: getProportionateScreenHeight(10),
            ),
            _transactionList()
          ],
        ),
      ),
    );
  }

  Widget _transactionList() {
    return Expanded(
      child: ListView.builder(
        itemCount: 15,
        itemBuilder: (context, index) {
          return _transactionListItem();
        },
      ),
    );
  }

  Widget _transactionListItem() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(getProportionateScreenHeight(5)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _itemImage(),
              SizedBox(
                width: getProportionateScreenHeight(10),
              ),
              _itemDetail(),
              _checkMark(),
              SizedBox(
                width: getProportionateScreenHeight(10),
              ),
            ],
          ),
        ));
  }

  Widget _itemImage() {
    return Container(
        child: CachedNetworkImage(
      height: getProportionateScreenHeight(65),
      width: getProportionateScreenWidth(65),
      fit: BoxFit.fill,
      imageUrl:
          "https://lh3.googleusercontent.com/uSLPHisaWJxapHGoTv5Zl57qB0kg9aZ3TZZ3nrpgQQf37rVdivBEJqOU1_osreBAfIh5-PjeDcG7S3AOfba3JSBnQ-ya=w1000",
      placeholder: (context, url) => Center(
        child: SizedBox(
            width: getProportionateScreenHeight(30),
            height: getProportionateScreenHeight(30),
            child: CircularProgressIndicator()),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
      imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
            bottomLeft: Radius.circular(getProportionateScreenHeight(5)),
            topLeft: Radius.circular(getProportionateScreenHeight(5))),
        image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
      )),
    ));
  }

  Widget _itemDetail() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _itemTitle(),
          SizedBox(
            height: getProportionateScreenHeight(2),
          ),
          _itemDate(),
          SizedBox(
            height: getProportionateScreenHeight(2),
          ),
          _itemAmount(),
        ],
      ),
    );
  }

  Widget _itemTitle() {
    return CustomText("2nd August 2020",
        textColor: grayColor, fontSize: getProportionateScreenHeight(12));
  }

  Widget _itemDate() {
    return CustomText("Woodland  Shoes",
        textColor: blackColor, fontSize: getProportionateScreenHeight(14));
  }

  Widget _itemAmount() {
    return CustomText('kes 2000.00',
        textColor: redColor, fontSize: getProportionateScreenHeight(12));
  }

  Widget _checkMark() {
    return Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
                width: 1, color: greenColor, style: BorderStyle.solid)),
        child: Icon(
          Icons.done,
          color: greenColor,
          size: getProportionateScreenHeight(20),
        ));
  }
}
