import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/helper/customTextField.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  String email;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: CustomText(
            forgotPassword.toUpperCase(),
            fontSize: getProportionateScreenHeight(18),
            textColor: whiteColor,
          ),
          centerTitle: true,
        ),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(getProportionateScreenHeight(10)),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: getProportionateScreenHeight(80),
                  ),
                  CustomText(
                    appName,
                    fontSize: getProportionateScreenHeight(30),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(80),
                  ),
                  CustomTextField(
                    hint: emailHint,
                    label: emailLabel,
                    keyboardType: TextInputType.emailAddress,
                    onSaved: (newValue) => email = newValue,
                    validator: (value) {
                      if (value.isEmpty) {
                        return emailNull;
                      } else if (!emailValidatorRegExp.hasMatch(value)) {
                        return emailInvalid;
                      }
                      return null;
                    },
                    onchange: (value) => {email = value},
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(15),
                  ),
                  CustomButton(
                      text: submit,
                      press: () {
                        _apiForgotPassword();
                      }),
                ],
              ),
            ),
          ),
        ));
  }

  Future<void> _apiForgotPassword() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      return;
    }

    if (!await ConstantFunctions.isOnline()) {
      ConstantFunctions.errorToast(noInternet);
      return;
    }

    //EasyLoading.show(status: loading);

    /*LoginData loginVo;
    LoginBloc loginBloc = LoginBloc();
    loginBloc.executeWorkerCategoryData(LoginRequest(email, password));

    loginBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        //if (value.data.Result.status) {
        loginVo = value.data.result;

        ConstantFunctions.successToast(value.data.message);
        */ /*Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => HomePage()));*/ /*
        */ /*} else {
          print("loginBloc error " + value.data.result.message);
          ConstFuns.errorToast(value.data.result.message);
        }*/ /*
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
        print("loginBloc error " + value.error.errorMessage);
        ConstantFunctions.errorToast(value.error.errorMessage);
      }
    });*/
  }
}
