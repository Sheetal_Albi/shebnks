import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customText.dart';

class ServicesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          bottomServices,
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10)),
          child: Column(
            children: [
              _serviceItem(
                  sheTransacts, sendDeposit, Icons.credit_card_outlined),
              _divider(),
              _serviceItem(
                  sheWaves, savings, Icons.account_balance_wallet_outlined),
              _divider(),
              _serviceItem(sheLoans, payLoan, Icons.close_fullscreen),
              _divider(),
              _serviceItem(
                  sheInsures, sheInsuresDesc, Icons.directions_car_outlined),
              _divider(),
              _serviceItem(sheManage, sheManageDesc, Icons.insert_link),
              _divider(),
              _serviceItem(sheIq, sheIqDesc, Icons.insert_chart_outlined),
              _divider(),
              _serviceItem(sheFund, sheFundDesc, Icons.payments),
              _divider(),
              _serviceItem(support, supportDesc, Icons.live_help_outlined),
            ],
          ),
        ),
      ),
    );
  }

  Widget _serviceItem(String title, String desc, IconData icon) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: getProportionateScreenHeight(20)),
      child: Row(
        children: [
          Icon(
            icon,
            size: getProportionateScreenHeight(35),
            color: redColor,
          ),
          SizedBox(
            width: getProportionateScreenWidth(10),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title,
                  fontSize: getProportionateScreenHeight(16),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(3),
                ),
                CustomText(
                  desc,
                  fontSize: getProportionateScreenHeight(12),
                  textColor: grayColor,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _divider() {
    return Container(
      width: double.infinity,
      height: 0.5,
      color: blackColor,
    );
  }
}
