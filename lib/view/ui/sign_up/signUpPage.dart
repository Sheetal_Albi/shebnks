import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shebnks/bloc/SignUpBloc.dart';
import 'package:shebnks/model/beans/EquityHoldersData.dart';
import 'package:shebnks/model/beans/SignUpData.dart';
import 'package:shebnks/model/remote/requests/SignUpRequest.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/KeyboardUtil.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/helper/customTextField.dart';
import 'package:shebnks/view/ui/dashboard/dashboardPage.dart';

class SignUpPage extends StatefulWidget {
  final List<File> files = new List<File>();

  // ignore: close_sinks
  final filesStream = StreamController<List<File>>();

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();

  String mobile,
      email,
      password,
      cPassword,
      businessName,
      businessAddress,
      businessValuation,
      businessNo,
      pinCode;

  List<EquityHolderData> equityHolderData = new List<EquityHolderData>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    equityHolderData.add(EquityHolderData());
  }

  @override
  void dispose() {
    widget.filesStream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: grayColor.withOpacity(0.2),
      appBar: AppBar(
        title: CustomText(
          signUp.toUpperCase(),
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(getProportionateScreenHeight(5)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _generalDetailsWidget(),
                  _businessDetailsWidget(),
                  _equityHolderDetails(),
                  _pickerWidget(),
                  SizedBox(
                    height: getProportionateScreenHeight(10),
                  ),
                  _bottomButtons(context),
                  SizedBox(
                    height: getProportionateScreenHeight(10),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _generalDetailsWidget() {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(getProportionateScreenHeight(10)),
        child: Column(
          children: [
            _title(generalDetails),
            CustomTextField(
              hint: mobileHint,
              label: mobileLabel,
              onSaved: (newValue) => mobile = newValue,
              keyboardType: TextInputType.phone,
              validator: (value) {
                if (value.isEmpty) {
                  return mobileNull;
                }
                return null;
              },
              onchange: (value) => {mobile = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: emailHint,
              label: emailLabel,
              onSaved: (newValue) => email = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return emailNull;
                } else if (!emailValidatorRegExp.hasMatch(value)) {
                  return emailInvalid;
                }
                return null;
              },
              onchange: (value) => {email = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: passwordHint,
              label: passwordLabel,
              isPassword: true,
              minLine: 1,
              onSaved: (newValue) => password = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return passwordNull;
                } else if (value.length < 8) {
                  return passwordInvalid;
                }
                return null;
              },
              onchange: (value) => {password = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: cPasswordHint,
              label: cPasswordLabel,
              isPassword: true,
              minLine: 1,
              onSaved: (newValue) => cPassword = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return cPasswordNull;
                } else if (value.length < 8) {
                  return cPasswordInvalid;
                }
                return null;
              },
              onchange: (value) => {cPassword = value},
            ),
          ],
        ),
      ),
    );
  }

  Widget _businessDetailsWidget() {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(getProportionateScreenHeight(10)),
        child: Column(
          children: [
            _title(businessDetails),
            CustomTextField(
              hint: businessNameHint,
              label: businessNameLabel,
              keyboardType: TextInputType.text,
              onSaved: (newValue) => businessName = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return businessNameNull;
                }
                return null;
              },
              onchange: (value) => {businessName = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: businessNoHint,
              label: businessNoLabel,
              onSaved: (newValue) => businessNo = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return businessNoNull;
                }
                return null;
              },
              onchange: (value) => {businessNo = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: businessAddressHint,
              label: businessAddressLabel,
              onSaved: (newValue) => businessAddress = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return businessAddressNull;
                }
                return null;
              },
              onchange: (value) => {businessAddress = value},
            ),
            SizedBox(
              height: getProportionateScreenHeight(15),
            ),
            CustomTextField(
              hint: businessValuationHint,
              label: businessValuationLabel,
              onSaved: (newValue) => businessValuation = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  return businessValuationNull;
                }
                return null;
              },
              onchange: (value) => {businessValuation = value},
            ),
          ],
        ),
      ),
    );
  }

  Widget _equityHolderDetails() {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(getProportionateScreenHeight(10)),
        child: Flex(direction: Axis.vertical, children: [
          _title(equityHolderDetails),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: equityHolderData.length,
            itemBuilder: (context, index) {
              return _holderItem(index);
            },
          ),
          SizedBox(
            height: getProportionateScreenHeight(15),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomButton(
                text: "-",
                press: () {
                  if (equityHolderData.length > 1) {
                    equityHolderData.removeLast();
                    setState(() {});
                  }
                },
                width: getProportionateScreenWidth(50),
              ),
              SizedBox(
                width: getProportionateScreenWidth(10),
              ),
              CustomButton(
                text: "+",
                press: () {
                  equityHolderData.add(EquityHolderData());
                  setState(() {});
                },
                width: getProportionateScreenWidth(50),
                bgColor: greenColor,
              ),
            ],
          )
        ]),
      ),
    );
  }

  Widget _holderItem(int index) {
    return Column(
      children: [
        SizedBox(
          height: getProportionateScreenHeight(5),
        ),
        CustomTextField(
          hint: equityHolderNameHint,
          label: equityHolderNameLabel,
          onSaved: (newValue) => equityHolderData[index].name = newValue,
          validator: (value) {
            if (value.isEmpty) {
              return equityHolderNameNull;
            }
            return null;
          },
          onchange: (value) => {equityHolderData[index].name = value},
        ),
        SizedBox(
          height: getProportionateScreenHeight(15),
        ),
        CustomTextField(
          hint: equityHolderPercentageHint,
          label: equityHolderPercentageHint,
          onSaved: (newValue) => equityHolderData[index].equity = newValue,
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return equityHolderPercentageNull;
            }
            return null;
          },
          onchange: (value) => {equityHolderData[index].equity = value},
        ),
        SizedBox(
          height: getProportionateScreenHeight(15),
        ),
        CustomTextField(
          hint: equityHolderRegisterNoHint,
          label: equityHolderRegisterNoLabel,
          onSaved: (newValue) => equityHolderData[index].no = newValue,
          validator: (value) {
            if (value.isEmpty) {
              return equityHolderNameNull;
            }
            return null;
          },
          onchange: (value) => {equityHolderData[index].no = value},
        ),
        SizedBox(
          height: getProportionateScreenHeight(10),
        ),
        Container(
          color: blackColor,
          height: 1,
          width: 100,
        ),
        SizedBox(
          height: getProportionateScreenHeight(5),
        ),
      ],
    );
  }

  Widget _pickerWidget() {
    return StreamBuilder(
        stream: widget.filesStream.stream,
        initialData: [],
        builder: (ctx, snapshot) {
          return ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return _docItem(snapshot.data[index], index);
            },
          );
        });
  }

  Widget _docItem(File file, int pos) {
    return Card(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(child: CustomText(file.uri.toString().split('/').last)),
          GestureDetector(
              onTap: () {
                widget.files.removeAt(pos);
                widget.filesStream.sink.add(widget.files);
              },
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Icon(
                  Icons.cancel,
                  color: redColor,
                  size: getProportionateScreenHeight(25),
                ),
              ))
        ],
      ),
    ));
  }

  Widget _title(String title) {
    return Padding(
      padding: EdgeInsets.only(bottom: getProportionateScreenHeight(15)),
      child: CustomText(
        title,
        fontSize: getProportionateScreenHeight(16),
      ),
    );
  }

  Widget _bottomButtons(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: GestureDetector(
          onTap: () async {
            FilePickerResult result =
                await FilePicker.platform.pickFiles(allowMultiple: true);

            if (result != null) {
              List<File> files =
                  result.paths.map((path) => File(path)).toList();

              for (int i = 0; i < files.length; i++) {
                widget.files.add(files[i]);
              }
              widget.filesStream.sink.add(widget.files);
            }
          },
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Icon(Icons.attachment_outlined),
                  CustomText(attachment)
                ]),
                CustomText(
                  optional,
                  fontSize: getProportionateScreenHeight(10),
                  textColor: blackColor.withOpacity(0.5),
                )
              ],
            ),
          ),
        )),
        CustomButton(
            text: signUp,
            width: getProportionateScreenWidth(150),
            press: () {
              _apiSignUp();
            })
      ],
    );
  }

  Future<void> _apiSignUp() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      return;
    }

    if (!await ConstantFunctions.isOnline()) {
      ConstantFunctions.errorToast(noInternet);
      return;
    }

    KeyboardUtil.hideKeyboard(context);
    EasyLoading.show(status: loading);

    SignUpData signUpData;
    SignUpBloc signUpBloc = SignUpBloc();

    signUpBloc.executeWorkerCategoryData(SignUpRequest(
      email: email,
      password: password,
      tel: mobile,
      businessName: businessName,
      businessAddress: businessAddress,
      businessRegistrationNum: businessNo,
      businessValuation: businessValuation,
      businessEquatyHolder: equityHolderData,
      country: "Kenya",
      businessDocument: widget.files,
    ));

    signUpBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();
        if (value.data.status) {
          signUpData = value.data.result;

          //ConstantFunctions.successToast(value.data.message);

          ConstantFunctions.saveSF(sfUserId, signUpData.user.id);
          ConstantFunctions.saveSF(
              sfUserData, signUpData.user.toJson().toString());
          ConstantFunctions.saveSF(sfToken, value.data.token);

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => DashboardPage(),
            ),
            (route) => false,
          );
        } else {
          print("signUpBloc error " + value.data.message);
          ConstantFunctions.errorToast(value.data.message);
        }
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
        print("signUpBloc error " + value.error.errorMessage);
        ConstantFunctions.errorToast(value.error.errorMessage);
      }
    });
  }
}
