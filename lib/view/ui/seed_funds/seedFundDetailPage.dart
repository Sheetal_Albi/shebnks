import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';

class SeedFundDetailPage extends StatefulWidget {
  @override
  _SeedFundDetailPageState createState() => _SeedFundDetailPageState();
}

class _SeedFundDetailPageState extends State<SeedFundDetailPage> {
  int currentStep = 4;

  goTo(int step) {
    setState(() => {currentStep = step});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          seedFundDetail,
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
      ),
      body: SafeArea(
        child: Column(children: <Widget>[
          Expanded(
            child: Stepper(
              currentStep: currentStep,
              /*onStepContinue: next,
              onStepCancel: cancel,*/
              onStepTapped: (step) => goTo(step),
              controlsBuilder: (BuildContext context,
                  {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                return Container();
              },
              steps: [
                Step(
                  title: CustomText("Created"),
                  subtitle: CustomText(
                    "12 Jan 2021",
                    fontSize: getProportionateScreenHeight(10),
                  ),
                  isActive: true,
                  state: StepState.complete,
                  content: _createdWidget(),
                ),
                Step(
                  title: CustomText("Review"),
                  subtitle: CustomText(
                    "12 Jan 2021",
                    fontSize: getProportionateScreenHeight(10),
                  ),
                  isActive: true,
                  state: StepState.complete,
                  content: _reviewWidget(),
                ),
                Step(
                  title: CustomText("Accept"),
                  subtitle: CustomText(
                    "13 Jan 2021",
                    fontSize: getProportionateScreenHeight(10),
                  ),
                  isActive: true,
                  state: StepState.complete,
                  content: _acceptWidget(),
                ),
                Step(
                  title: CustomText("Reject"),
                  subtitle: CustomText(
                    "15 Jan 2021",
                    fontSize: getProportionateScreenHeight(10),
                  ),
                  isActive: true,
                  state: StepState.complete,
                  content: _rejectWidget(),
                ),
                Step(
                  title: CustomText("Modify"),
                  subtitle: CustomText(
                    "16 Jan 2021",
                    fontSize: getProportionateScreenHeight(10),
                  ),
                  isActive: true,
                  state: StepState.complete,
                  content: _modifyWidget(),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  Widget _createdWidget() {
    return Row(
      children: [
        Image.asset(
          "assets/images/apply.png",
          height: getProportionateScreenHeight(40),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Flexible(
            child: CustomText(
          "You applied for equity seed fund on 12 Jan 2021",
          fontSize: getProportionateScreenHeight(14),
        ))
      ],
    );
  }

  Widget _reviewWidget() {
    return Row(
      children: [
        Image.asset(
          "assets/images/review.png",
          height: getProportionateScreenHeight(40),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Flexible(
            child: CustomText(
          "You application is under review.",
          fontSize: getProportionateScreenHeight(14),
        ))
      ],
    );
  }

  Widget _acceptWidget() {
    return Row(
      children: [
        Image.asset(
          "assets/images/accept.png",
          height: getProportionateScreenHeight(40),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Flexible(
            child: CustomText(
          "You application is accepted.",
          fontSize: getProportionateScreenHeight(14),
        ))
      ],
    );
  }

  Widget _rejectWidget() {
    return Row(
      children: [
        Image.asset(
          "assets/images/reject.png",
          height: getProportionateScreenHeight(40),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Flexible(
            child: CustomText(
          "You application is rejected.",
          fontSize: getProportionateScreenHeight(14),
        ))
      ],
    );
  }

  Widget _modifyWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Image.asset(
              "assets/images/modify.png",
              height: getProportionateScreenHeight(40),
            ),
            SizedBox(
              width: getProportionateScreenWidth(10),
            ),
            Flexible(
                child: CustomText(
              "We are not able to pay you 1M for 10% equity. We only able to give you 80k for it.",
              fontSize: getProportionateScreenHeight(12),
            ))
          ],
        ),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Updated Amount: ",
                style: TextStyle(
                  color: blackColor,
                  fontSize: getProportionateScreenHeight(14),
                ),
              ),
              TextSpan(
                text: "80k ",
                style: TextStyle(
                  color: blackColor,
                  fontSize: getProportionateScreenHeight(14),
                ),
              ),
              TextSpan(
                text: "1M",
                style: TextStyle(
                  color: blackColor.withOpacity(0.5),
                  fontSize: getProportionateScreenHeight(14),
                  decoration: TextDecoration.lineThrough,
                ),
              ),
            ],
          ),
        ),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Updated Equity: ",
                style: TextStyle(
                  color: blackColor,
                  fontSize: getProportionateScreenHeight(14),
                ),
              ),
              TextSpan(
                text: "10% ",
                style: TextStyle(
                  color: blackColor,
                  fontSize: getProportionateScreenHeight(14),
                ),
              ),
              TextSpan(
                text: "",
                style: TextStyle(
                  color: blackColor.withOpacity(0.5),
                  fontSize: getProportionateScreenHeight(14),
                  decoration: TextDecoration.lineThrough,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        CustomText(
          "Do you want to accept and update your application?",
          fontSize: getProportionateScreenHeight(12),
        ),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomButton(
                text: "Accept",
                press: () {},
                bgColor: greenColor,
                width: getProportionateScreenWidth(90)),
            CustomButton(
                text: "Modify",
                press: () {},
                bgColor: grayColor,
                textColor: blackColor,
                width: getProportionateScreenWidth(90)),
            CustomButton(
                text: "Reject",
                press: () {},
                width: getProportionateScreenWidth(90)),
          ],
        )
      ],
    );
  }
}
