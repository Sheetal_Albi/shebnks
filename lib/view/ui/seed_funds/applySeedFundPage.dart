import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/helper/customTextField.dart';

class ApplySeedFundPage extends StatefulWidget {
  final List<File> files = new List<File>();

  // ignore: close_sinks
  final filesStream = StreamController<List<File>>();

  @override
  _ApplySeedFundPageState createState() => _ApplySeedFundPageState();
}

class _ApplySeedFundPageState extends State<ApplySeedFundPage> {
  String title, amount, equity, desc;

  @override
  void dispose() {
    widget.filesStream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          applySeedFund.toUpperCase(),
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(getProportionateScreenHeight(10)),
          child: Column(
            children: [
              _titleWidget(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              _amountWidget(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              _equityWidget(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              _descWidget(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              _filesListWidget(),
              SizedBox(
                height: getProportionateScreenHeight(5),
              ),
              _selectButtonWidget(),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              _applyButtonWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget _titleWidget() {
    return CustomTextField(
      hint: titleText,
      label: titleText,
      onSaved: (newValue) => title = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return titleNull;
        }
        return null;
      },
      onchange: (value) => {title = value},
    );
  }

  Widget _amountWidget() {
    return CustomTextField(
      hint: amountText,
      label: amountText,
      onSaved: (newValue) => amount = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return amountNull;
        }
        return null;
      },
      onchange: (value) => {amount = value},
    );
  }

  Widget _equityWidget() {
    return CustomTextField(
      hint: equityText,
      label: equityText,
      keyboardType: TextInputType.number,
      onSaved: (newValue) => equity = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return equityNull;
        }
        return null;
      },
      onchange: (value) => {equity = value},
    );
  }

  Widget _descWidget() {
    return CustomTextField(
      hint: descText,
      label: descText,
      minLine: 5,
      action: TextInputAction.newline,
      onSaved: (newValue) => desc = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return descNull;
        }
        return null;
      },
      onchange: (value) => {desc = value},
    );
  }

  Widget _filesListWidget() {
    return StreamBuilder(
        stream: widget.filesStream.stream,
        initialData: [],
        builder: (ctx, snapshot) {
          return ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return _docItem1(snapshot.data[index], index);
            },
          );
        });
  }

  Widget _selectButtonWidget() {
    return GestureDetector(
      onTap: () async {
        FilePickerResult result =
            await FilePicker.platform.pickFiles(allowMultiple: true);

        if (result != null) {
          List<File> files = result.paths.map((path) => File(path)).toList();

          for (int i = 0; i < files.length; i++) {
            widget.files.add(files[i]);
          }
          widget.filesStream.sink.add(widget.files);
        } else {
          // User canceled the picker
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: CustomText(
          selectDocs,
          fontSize: getProportionateScreenHeight(16),
          textDecoration: TextDecoration.underline,
        ),
      ),
    );
  }

  Widget _docItem1(File file, int pos) {
    return Card(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(child: CustomText(file.uri.toString().split('/').last)),
          GestureDetector(
              onTap: () {
                widget.files.removeAt(pos);
                widget.filesStream.sink.add(widget.files);
              },
              child: Icon(
                Icons.cancel,
                color: redColor,
                size: getProportionateScreenHeight(25),
              ))
        ],
      ),
    ));
  }

  Widget _applyButtonWidget() {
    return CustomButton(text: apply.toUpperCase(), press: () {});
  }
}
