import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shebnks/bloc/SeedsBloc.dart';
import 'package:shebnks/model/beans/SeedsData.dart';
import 'package:shebnks/model/remote/requests/SeedsRequest.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customText.dart';
import 'package:shebnks/view/ui/seed_funds/applySeedFundPage.dart';
import 'package:shebnks/view/ui/seed_funds/seedFundDetailPage.dart';

class SeedFundsListPage extends StatefulWidget {
  @override
  _SeedFundsListPageState createState() => _SeedFundsListPageState();
}

class _SeedFundsListPageState extends State<SeedFundsListPage> {
  SeedsData seedsData = new SeedsData();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    seedsData.seedfund = new List<Seedfund>();
    _apiGetSeeds();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayColor.withOpacity(0.2),
      body: Column(
        children: [_headerWidget(), _listWidget()],
      ),
      floatingActionButton: _fabWidget(context),
    );
  }

  Widget _headerWidget() {
    return Container(
      padding: EdgeInsets.all(getProportionateScreenHeight(15)),
      color: redColor,
      child: Row(
        children: [
          SvgPicture.asset(
            "assets/svg/speacker.svg",
            height: getProportionateScreenHeight(70),
          ),
          SizedBox(
            width: getProportionateScreenWidth(10),
          ),
          Flexible(
              child: CustomText(
                dotConnectAfrica,
                textColor: whiteColor,
                fontSize: getProportionateScreenHeight(15),
              ))
        ],
      ),
    );
  }

  Widget _fabWidget(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ApplySeedFundPage()));
      },
      backgroundColor: redColor,
      child: Center(
          child: CustomText(
            apply,
            textColor: whiteColor,
            fontSize: getProportionateScreenHeight(12),
          )),
    );
  }

  Widget _listWidget() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(
            left: getProportionateScreenHeight(8),
            right: getProportionateScreenHeight(8)),
        child: seedsData.seedfund.length > 0
            ? ListView.builder(
                itemCount: seedsData.seedfund.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  return _listItem(index);
                },
              )
            : _noDataWidget(),
      ),
    );
  }

  Widget _listItem(int index) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => SeedFundDetailPage()));
      },
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(getProportionateScreenHeight(5)),
          ),
          margin:
          EdgeInsets.symmetric(vertical: getProportionateScreenHeight(5)),
          child: Padding(
            padding: EdgeInsets.all(getProportionateScreenHeight(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  seedsData.seedfund[index].title,
                  fontSize: getProportionateScreenHeight(15),
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(
                  height: getProportionateScreenHeight(5),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      "Amount : " + seedsData.seedfund[index].amount,
                      fontSize: getProportionateScreenHeight(13),
                    ),
                    CustomText(
                      "Equity : " + seedsData.seedfund[index].equity,
                      fontSize: getProportionateScreenHeight(13),
                    ),
                  ],
                ),
                SizedBox(
                  height: getProportionateScreenHeight(5),
                ),
                CustomText(
                  seedsData.seedfund[index].description,
                  fontSize: getProportionateScreenHeight(11),
                  textColor: blackColor.withOpacity(0.5),
                ),
              ],
            ),
          )),
    );
  }

  Widget _noDataWidget() {
    return Center(
      child: CustomText(
        "No data added",
        fontSize: getProportionateScreenHeight(18),
      ),
    );
  }

  void _apiGetSeeds() {
    EasyLoading.show(status: loading);

    SeedsBloc seedsBloc = SeedsBloc();
    seedsBloc.executeWorkerCategoryData(SeedsRequest("hu8gt76t7t7"));

    seedsBloc.subject.listen((value) {
      if (value.status == Status.LOADING) {
      } else if (value.status == Status.SUCCESS) {
        EasyLoading.dismiss();

        if (value.data.status) {
          seedsData = value.data.result;

          setState(() {});
        } else {
          print("seedsBloc error " + value.data.message);
          ConstantFunctions.errorToast(value.data.message);
        }
      } else if (value.status == Status.ERROR) {
        EasyLoading.dismiss();
        print("seedsBloc error " + value.error.errorMessage);
        ConstantFunctions.errorToast(value.error.errorMessage);
      }
    });
  }
}
