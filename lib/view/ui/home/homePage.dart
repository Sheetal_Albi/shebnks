import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/helper/customButton.dart';
import 'package:shebnks/view/helper/customText.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CustomText(
          homeTitle,
          fontSize: getProportionateScreenHeight(18),
          textColor: whiteColor,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications),
            tooltip: notification,
            onPressed: () {
              //save();
            },
          ),
          IconButton(
            icon: Icon(Icons.account_circle_rounded),
            tooltip: profile,
            onPressed: () {
              //save();
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _headerWidget(),
            _transactionWidget(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _settingModalBottomSheet(context);
        },
        backgroundColor: redColor,
        child: Icon(
          Icons.add,
          color: whiteColor,
          size: getProportionateScreenHeight(30),
        ),
      ),
    );
  }

  Widget _headerWidget() {
    return Container(
      color: grayColor.withOpacity(0.1),
      width: double.infinity,
      padding: EdgeInsets.all(getProportionateScreenHeight(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          CustomText(
            yourWallet,
            textAlign: TextAlign.center,
            fontSize: getProportionateScreenHeight(18),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: getProportionateScreenHeight(20),
                bottom: getProportionateScreenHeight(10)),
            child: Icon(
              Icons.account_balance_wallet_outlined,
              color: redColor,
              size: getProportionateScreenHeight(40),
            ),
          ),
          CustomText(
            balance,
            textAlign: TextAlign.center,
            fontSize: getProportionateScreenHeight(18),
          ),
          SizedBox(
            height: getProportionateScreenHeight(5),
          ),
          CustomText(
            "\$ 2800",
            textAlign: TextAlign.center,
            fontSize: getProportionateScreenHeight(18),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          CustomButton(
            text: sendMoney.toUpperCase(),
            press: () {},
            icon: Icons.language,
            width: getProportionateScreenHeight(200),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          CustomButton(
            text: mPesaPay.toUpperCase(),
            press: () {},
            icon: Icons.article_outlined,
            textColor: blackColor,
            bgColor: greenColor,
            width: getProportionateScreenHeight(200),
          ),
          SizedBox(
            height: getProportionateScreenHeight(5),
          ),
        ],
      ),
    );
  }

  Widget _transactionWidget() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(getProportionateScreenHeight(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          CustomText(
            bottomTransaction,
            textAlign: TextAlign.center,
            fontSize: getProportionateScreenHeight(22),
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          _tabBars(),
          _transactionList()
        ],
      ),
    );
  }

  Widget _tabBars() {
    return TabBar(
      controller: TabController(length: 4, vsync: this),
      labelColor: redColor,
      unselectedLabelColor: redColor,
      indicatorColor: redColor,
      onTap: (index) {},
      tabs: [
        new Tab(
          text: today,
        ),
        new Tab(
          text: week,
        ),
        new Tab(
          text: month,
        ),
        new Tab(
          text: year,
        ),
      ],
    );
  }

  Widget _transactionList() {
    return Flex(
      direction: Axis.vertical,
      children: [
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 10,
          itemBuilder: (context, index) {
            return _transactionListItem();
          },
        )
      ],
    );
  }

  Widget _transactionListItem() {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(getProportionateScreenHeight(5)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _itemImage(),
              SizedBox(
                width: getProportionateScreenHeight(10),
              ),
              _itemDetail(),
              _checkMark(),
              SizedBox(
                width: getProportionateScreenHeight(10),
              ),
            ],
          ),
        ));
  }

  Widget _itemImage() {
    return Container(
        child: CachedNetworkImage(
      height: getProportionateScreenHeight(65),
      width: getProportionateScreenWidth(65),
      fit: BoxFit.fill,
      imageUrl:
          "https://lh3.googleusercontent.com/uSLPHisaWJxapHGoTv5Zl57qB0kg9aZ3TZZ3nrpgQQf37rVdivBEJqOU1_osreBAfIh5-PjeDcG7S3AOfba3JSBnQ-ya=w1000",
      placeholder: (context, url) => Center(
        child: SizedBox(
            width: getProportionateScreenHeight(30),
            height: getProportionateScreenHeight(30),
            child: CircularProgressIndicator()),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
      imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
            bottomLeft: Radius.circular(getProportionateScreenHeight(5)),
            topLeft: Radius.circular(getProportionateScreenHeight(5))),
        image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
      )),
    ));
  }

  Widget _itemDetail() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _itemTitle(),
          SizedBox(
            height: getProportionateScreenHeight(2),
          ),
          _itemDate(),
          SizedBox(
            height: getProportionateScreenHeight(2),
          ),
          _itemAmount(),
        ],
      ),
    );
  }

  Widget _itemTitle() {
    return CustomText("Woodland  Shoes",
        textColor: blackColor, fontSize: getProportionateScreenHeight(14));
  }

  Widget _itemDate() {
    return CustomText("2nd August 2020",
        textColor: grayColor, fontSize: getProportionateScreenHeight(12));
  }

  Widget _itemAmount() {
    return CustomText('kes 2000.00',
        textColor: redColor, fontSize: getProportionateScreenHeight(12));
  }

  Widget _checkMark() {
    return Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
                width: 1, color: greenColor, style: BorderStyle.solid)),
        child: Icon(
          Icons.done,
          color: greenColor,
          size: getProportionateScreenHeight(20),
        ));
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomText(
                        "Choose Service",
                        fontSize: getProportionateScreenHeight(20),
                      ),
                      Icon(
                        Icons.cancel,
                        color: redColor,
                        size: getProportionateScreenHeight(25),
                      ),
                    ],
                  ),
                  new ListTile(
                      leading: new Icon(Icons.music_note),
                      title: new Text('Music'),
                      onTap: () => {}),
                  new ListTile(
                    leading: new Icon(Icons.videocam),
                    title: new Text('Video'),
                    onTap: () => {},
                  ),
                ],
              ),
            ),
          );
        });
  }
}
