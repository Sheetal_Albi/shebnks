import 'package:flutter/material.dart';
import 'package:shebnks/bloc/BottomNavBarBloc.dart';
import 'package:shebnks/view/constants/Constant.dart';
import 'package:shebnks/view/constants/SizeConfig.dart';
import 'package:shebnks/view/ui/home/homePage.dart';
import 'package:shebnks/view/ui/profile/profilePage.dart';
import 'package:shebnks/view/ui/seed_funds/seedFundsListPage.dart';
import 'package:shebnks/view/ui/services/servicesPage.dart';
import 'package:shebnks/view/ui/transactions/transactionsPage.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  BottomNavBarBloc _bottomNavBarBloc;

  @override
  void initState() {
    super.initState();
    _bottomNavBarBloc = BottomNavBarBloc();
  }

  @override
  void dispose() {
    _bottomNavBarBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: StreamBuilder<NavBarItem>(
          stream: _bottomNavBarBloc.itemStream,
          initialData: _bottomNavBarBloc.defaultItem,
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
            switch (snapshot.data) {
              case NavBarItem.Home:
                return HomePage();
              case NavBarItem.Transactions:
                return TransactionsPage();
              case NavBarItem.Services:
                return ServicesPage();
              case NavBarItem.SeedFunds:
                return SeedFundsListPage();
              case NavBarItem.Profile:
                return ProfilePage();
            }
          },
        ),
      ),
      bottomNavigationBar: StreamBuilder(
        stream: _bottomNavBarBloc.itemStream,
        initialData: _bottomNavBarBloc.defaultItem,
        builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
          return BottomNavigationBar(
            backgroundColor: whiteColor,
            //selectedItemColor: redColor,
            unselectedItemColor: grayColor,
            type: BottomNavigationBarType.fixed,
            currentIndex: snapshot.data.index,
            onTap: _bottomNavBarBloc.pickItem,
            items: [
              BottomNavigationBarItem(
                label: bottomHome,
                icon: Icon(Icons.home),
              ),
              BottomNavigationBarItem(
                label: bottomTransaction,
                icon: Icon(Icons.show_chart_outlined),
              ),
              BottomNavigationBarItem(
                label: bottomServices,
                icon: Icon(Icons.format_list_bulleted),
              ),
              BottomNavigationBarItem(
                label: bottomSeedFund,
                icon: Icon(Icons.payments),
              ),
              BottomNavigationBarItem(
                label: bottomProfile,
                icon: Icon(Icons.person_rounded),
              ),
            ],
          );
        },
      ),
    );
  }
}
