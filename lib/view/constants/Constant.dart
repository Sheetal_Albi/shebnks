import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Colors
const redColor = Color(0xFFff6666);
const whiteColor = Color(0xFFFFFFFF);
const blackColor = Color(0xFF000000);
const grayColor = Color(0xFFa0a0a0);
const greenColor = Color(0xFF2dd36f);

// Strings
const appName = "SHE Bnks";
const yourWallet = "Your Wallet";
const profile = "Profile";
const notification = "Notification";
const balance = "Balance";
const today = "TODAY";
const week = "WEEK";
const month = "MONTH";
const year = "YEAR";
const sheTransacts = "SHE Transacts";
const sendDeposit = "Send , Deposit, Withdraw, Balance & Payment";
const sheWaves = "SHE Waves";
const savings = "Savings";
const sheLoans = "SHE Loans";
const payLoan = "Request & Pay Loan";
const sheInsures = "SHE Insures";
const sheInsuresDesc = "Register, Pay & Claim Insurance benefits";
const sheManage = "SHE Manage Account";
const sheManageDesc = "Link your bank account(s)";
const sheIq = "SHE IQ";
const sheIqDesc = "Take a survey to take your loan";
const sheFund = "SHE FUND";
const sheFundDesc = "Apply for SeedFund for your startup";
const support = "Support";
const supportDesc = "Need help? Reach us";
const dotConnectAfrica =
    "DotConnectAfrica SeedFund Empowering the African Woman";
const apply = "Apply";
const openSeedFund = "Open SeedFund";
const pastSeedFund = "Past SeedFunds";
const profileSetting = "Profile Settings";
const profileSettingDesc = "Update and modify your profile";
const accountManagement = "Account Management";
const accountManagementDesc = "Link, Manage your SHEBnks account";
const createSeedFund = "Create SeedFund";
const createSeedFundDesc = "Create SeedFund details for an open SeedFund";
const seedFundApplication = "SeedFund Applications";
const seedFundApplicationDesc = "View current SeedFund applications";
const privacy = "Privacy";
const privacyDesc = "Change your password";
const notificationSettings = "Notification Settings";
const notificationSettingsDesc = "Change your notification settings";
const logout = "Log Out";
const logoutDesc = "Log out your account";
const signUp = "Sign Up";
const forgotPassword = "Forgot Password";
const applySeedFund = "Apply Seed Fund";
const emailHint = "Enter valid email address";
const emailLabel = "Email";
const mobileHint = "Enter valid mobile number";
const mobileLabel = "Mobile";
const passwordHint = "Enter minimum 8 character password";
const passwordLabel = "Password";
const cPasswordHint = "Enter minimum 8 character password";
const cPasswordLabel = "Confirm Password";
const businessNameHint = "Enter your business name";
const businessNameLabel = "Business Name";
const businessAddressHint = "Enter valid business address";
const businessAddressLabel = "Business Address";
const businessNoHint = "Enter valid business number";
const businessNoLabel = "Business Number";
const businessValuationHint = "Enter business valuation";
const businessValuationLabel = "Business Valuation";
const generalDetails = "General Details";
const businessDetails = "Business Details";
const equityHolderDetails = "Equity Holders Detail";
const documentUpload = "Document Upload";
const equityHolderNameHint = "Enter equity holder name";
const equityHolderNameLabel = "Name";
const equityHolderRegisterNoHint = "Enter equity holder register number";
const equityHolderRegisterNoLabel = "Register Number";
const equityHolderPercentageHint = "Enter equity of holder (%)";
const equityHolderPercentageNoLabel = "Equity";
const titleText = "Title";
const equityText = "Equity";
const amountText = "Amount";
const descText = "Description";
const seedFundDetail = "Seed Fund Detail";
final String loading = "Loading...";
final String noInternet = "No Internet Connection";
final String submit = "Submit";
final String attachment = "Attachment";
final String optional = "(Optional)";
final String login = "Login";
final String dontHaveAcc = "Don't have an account? ";
final String registerHere = "Register here";
final String forgotPassword_ = "Forgot Password?";

//Button text
const sendMoney = "Send Money";
const mPesaPay = "M-PESA PAYBILL";
const selectDocs = "Select Docs";

//Page title
const homeTitle = "SHEBnks";

// Bottom bar labels
const bottomHome = "Home";
const bottomTransaction = "Transactions";
const bottomServices = "Services";
const bottomSeedFund = "Seed Fund";
const bottomProfile = "Profile";

//error strings
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final String emailNull = "Email can not be blank";
final String mobileNull = "Mobile can not be blank";
final String emailInvalid = "Enter valid email address";
final String passwordNull = "Password can not be blank";
final String passwordInvalid = "Incorrect password";
final String cPasswordNull = "Confirm password can not be blank";
final String cPasswordInvalid = "Incorrect confirm password";
final String businessNameNull = "Business name can not be blank";
final String businessAddressNull = "Business address can not be blank";
final String businessNoNull = "Business number can not be blank";
final String businessValuationNull = "Business valuation can not be blank";
final String equityHolderNameNull = "Name can not be empty";
final String equityHolderPercentageNull = "Percentage can not be empty";
final String titleNull = "Title can not be empty";
final String equityNull = "Equity can not be empty";
final String amountNull = "Amount can not be empty";
final String descNull = "Description can not be empty";

//SharePreferences Keys
final String sfUserId = "userId";
final String sfUserData = "userData";
final String sfToken = "token";

class ConstantFunctions {
  static Future<bool> isOnline() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      }
    } on SocketException catch (_) {
      print('not connected');
      return false;
    }
    return false;
  }

  static void successToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: greenColor,
        textColor: whiteColor,
        fontSize: 16.0);
  }

  static void errorToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: redColor,
        textColor: whiteColor,
        fontSize: 16.0);
  }

  static void saveSF(String key, String val) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, val);
  }

  static Future<String> getSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);
    return stringValue;
  }
}
