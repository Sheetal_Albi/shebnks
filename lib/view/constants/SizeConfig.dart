import 'package:flutter/material.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenHeight(double inputHeight) {
  double designHeight = 812.0;
  // 812 is the layout height that designer use
  if (SizeConfig.orientation == Orientation.portrait) {
    return (inputHeight / designHeight) * SizeConfig.screenHeight;
  } else {
    return (inputHeight / designHeight) * SizeConfig.screenWidth;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenWidth(double inputWidth) {
  double designWidth = 375.0;
  // 375 is the layout width that designer use
  if (SizeConfig.orientation == Orientation.portrait) {
    return (inputWidth / designWidth) * SizeConfig.screenWidth;
  } else {
    return (inputWidth / designWidth) * SizeConfig.screenHeight;
  }
}
