import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shebnks/view/helper/customTheme.dart';
import 'package:shebnks/view/ui/dashboard/dashboardPage.dart';

void main() async {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Errol',
    theme: customTheme(),
    home: DashboardPage(),
    // home: LoginPage(),
    builder: EasyLoading.init(),
  ));
}
