import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/LoginRequest.dart';
import 'package:shebnks/model/remote/response/LoginResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/model/repo/LoginRepository.dart';

class LoginBloc {
  final LoginRepository _paymentPlanRepository = LoginRepository();

  final BehaviorSubject<ApiResponse<LoginResponse>> _subjectWorkerCategoryData =
      BehaviorSubject<ApiResponse<LoginResponse>>();

  executeWorkerCategoryData(LoginRequest workerCategoryRequest) {
    _paymentPlanRepository.executeWorkerCategoryData(
        workerCategoryRequest, _subjectWorkerCategoryData);
  }

  BehaviorSubject<ApiResponse<LoginResponse>> get subject =>
      _subjectWorkerCategoryData;

  /// functions that used to  close the Subject stream
  disposeWorkerCategoryDataSubject() {
    _subjectWorkerCategoryData.close();
  }
}
