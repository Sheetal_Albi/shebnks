import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SignUpRequest.dart';
import 'package:shebnks/model/remote/response/SignUpResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/model/repo/SignUpRepository.dart';

class SignUpBloc {
  final SignUpRepository _paymentPlanRepository = SignUpRepository();

  final BehaviorSubject<ApiResponse<SignUpResponse>>
      _subjectWorkerCategoryData =
      BehaviorSubject<ApiResponse<SignUpResponse>>();

  executeWorkerCategoryData(SignUpRequest workerCategoryRequest) {
    _paymentPlanRepository.executeWorkerCategoryData(
        workerCategoryRequest, _subjectWorkerCategoryData);
  }

  BehaviorSubject<ApiResponse<SignUpResponse>> get subject =>
      _subjectWorkerCategoryData;

  /// functions that used to  close the Subject stream
  disposeWorkerCategoryDataSubject() {
    _subjectWorkerCategoryData.close();
  }
}
