import 'dart:async';

enum NavBarItem { Home, Transactions, Services, SeedFunds, Profile }

class BottomNavBarBloc {
  final StreamController<NavBarItem> _navBarController =
      StreamController<NavBarItem>.broadcast();

  NavBarItem defaultItem = NavBarItem.Home;

  Stream<NavBarItem> get itemStream => _navBarController.stream;

  void pickItem(int i) {
    switch (i) {
      case 0:
        _navBarController.sink.add(NavBarItem.Home);
        break;
      case 1:
        _navBarController.sink.add(NavBarItem.Transactions);
        break;
      case 2:
        _navBarController.sink.add(NavBarItem.Services);
        break;
      case 3:
        _navBarController.sink.add(NavBarItem.SeedFunds);
        break;
      case 4:
        _navBarController.sink.add(NavBarItem.Profile);
        break;
    }
  }

  close() {
    _navBarController?.close();
  }
}
