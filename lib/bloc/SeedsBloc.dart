import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SeedsRequest.dart';
import 'package:shebnks/model/remote/response/SeedsResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/model/repo/SeedsRepository.dart';

class SeedsBloc {
  final SeedsRepository _paymentPlanRepository = SeedsRepository();

  final BehaviorSubject<ApiResponse<SeedsResponse>> _subjectWorkerCategoryData =
      BehaviorSubject<ApiResponse<SeedsResponse>>();

  executeWorkerCategoryData(SeedsRequest workerCategoryRequest) {
    _paymentPlanRepository.executeWorkerCategoryData(
        workerCategoryRequest, _subjectWorkerCategoryData);
  }

  BehaviorSubject<ApiResponse<SeedsResponse>> get subject =>
      _subjectWorkerCategoryData;

  /// functions that used to  close the Subject stream
  disposeWorkerCategoryDataSubject() {
    _subjectWorkerCategoryData.close();
  }
}
