class EquityHolderData {
  String name;
  String no;
  String equity;

  EquityHolderData({this.name, this.no, this.equity});

  EquityHolderData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    no = json['no'];
    equity = json['equity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['no'] = this.no;
    data['equity'] = this.equity;
    return data;
  }
}