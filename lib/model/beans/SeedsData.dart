import 'package:shebnks/model/beans/BaseData.dart';

class SeedsData extends BaseData {
  List<Seedfund> seedfund = new List<Seedfund>();

  SeedsData({this.seedfund});

  SeedsData.fromJson(Map<String, dynamic> json) {
    if (json['seedfund'] != null) {
      seedfund = new List<Seedfund>();
      json['seedfund'].forEach((v) {
        seedfund.add(new Seedfund.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.seedfund != null) {
      data['seedfund'] = this.seedfund.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Seedfund {
  List<String> businessDocument;
  String status;
  String sId;
  String title;
  String amount;
  String equity;
  String description;
  String dateCreated;
  String dateUpdated;
  int iV;

  Seedfund(
      {this.businessDocument,
      this.status,
      this.sId,
      this.title,
      this.amount,
      this.equity,
      this.description,
      this.dateCreated,
      this.dateUpdated,
      this.iV});

  Seedfund.fromJson(Map<String, dynamic> json) {
    businessDocument = json['business_document'].cast<String>();
    status = json['status'];
    sId = json['_id'];
    title = json['title'];
    amount = json['amount'];
    equity = json['equity'];
    description = json['description'];
    dateCreated = json['date_created'];
    dateUpdated = json['date_updated'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['business_document'] = this.businessDocument;
    data['status'] = this.status;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['amount'] = this.amount;
    data['equity'] = this.equity;
    data['description'] = this.description;
    data['date_created'] = this.dateCreated;
    data['date_updated'] = this.dateUpdated;
    data['__v'] = this.iV;
    return data;
  }
}
