class UserData {
  List<BusinessEquatyHolder> businessEquatyHolder;
  List<String> businessDocument;
  String role;
  String id;
  String tel;
  String businessName;
  String businessAddress;
  String businessRegistrationNum;
  String businessValuation;
  String email;
  String country;
  String password;
  String date;
  int iV;

  UserData(
      {this.businessEquatyHolder,
      this.businessDocument,
      this.role,
      this.id,
      this.tel,
      this.businessName,
      this.businessAddress,
      this.businessRegistrationNum,
      this.businessValuation,
      this.email,
      this.country,
      this.password,
      this.date,
      this.iV});

  UserData.fromJson(Map<String, dynamic> json) {
    if (json['business_equaty_holder'] != null) {
      businessEquatyHolder = new List<BusinessEquatyHolder>();
      json['business_equaty_holder'].forEach((v) {
        businessEquatyHolder.add(new BusinessEquatyHolder.fromJson(v));
      });
    }
    businessDocument = json['business_document'].cast<String>();
    role = json['role'];
    id = json['_id'];
    tel = json['tel'];
    businessName = json['business_name'];
    businessAddress = json['business_address'];
    businessRegistrationNum = json['business_registration_num'];
    businessValuation = json['business_valuation'];
    email = json['email'];
    country = json['country'];
    password = json['password'];
    date = json['date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.businessEquatyHolder != null) {
      data['business_equaty_holder'] =
          this.businessEquatyHolder.map((v) => v.toJson()).toList();
    }
    data['business_document'] = this.businessDocument;
    data['role'] = this.role;
    data['_id'] = this.id;
    data['tel'] = this.tel;
    data['business_name'] = this.businessName;
    data['business_address'] = this.businessAddress;
    data['business_registration_num'] = this.businessRegistrationNum;
    data['business_valuation'] = this.businessValuation;
    data['email'] = this.email;
    data['country'] = this.country;
    data['password'] = this.password;
    data['date'] = this.date;
    data['__v'] = this.iV;
    return data;
  }
}

class BusinessEquatyHolder {
  String name;
  String no;
  String equity;

  BusinessEquatyHolder({this.name, this.no, this.equity});

  BusinessEquatyHolder.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    no = json['no'];
    equity = json['equity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['no'] = this.no;
    data['equity'] = this.equity;
    return data;
  }
}
