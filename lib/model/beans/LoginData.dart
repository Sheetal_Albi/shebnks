import 'package:shebnks/model/beans/BaseData.dart';
import 'package:shebnks/model/beans/UserData.dart';

class LoginData extends BaseData {
  UserData user;

  LoginData({this.user});

  LoginData.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.user != null) {
      data['user'] = this.user.toJson();
    }

    return data;
  }
}
