import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SeedsRequest.dart';
import 'package:shebnks/model/remote/response/SeedsResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';

import 'calls/SeedsCall.dart';

class SeedsRepository {
  executeWorkerCategoryData(SeedsRequest workerCategoryRequest,
      BehaviorSubject<ApiResponse<SeedsResponse>> responseSubject) {
    new SeedsCall(workerCategoryRequest, responseSubject).execute();
  }
}
