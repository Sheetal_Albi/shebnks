import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/LoginRequest.dart';
import 'package:shebnks/model/remote/response/LoginResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';

import 'calls/LoginCall.dart';

class LoginRepository {
  executeWorkerCategoryData(LoginRequest workerCategoryRequest,
      BehaviorSubject<ApiResponse<LoginResponse>> responseSubject) {
    new LoginCall(workerCategoryRequest, responseSubject).execute();
  }
}
