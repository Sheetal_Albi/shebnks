import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SeedsRequest.dart';
import 'package:shebnks/model/remote/response/SeedsResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/model/remote/util/ApiService.dart';
import 'package:shebnks/model/remote/util/DataFetchCall.dart';

class SeedsCall extends DataFetchCall<SeedsResponse> {
  SeedsRequest _request;

  SeedsCall(SeedsRequest request,
      BehaviorSubject<ApiResponse<SeedsResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.getSeedsData(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(SeedsResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  SeedsResponse parseJson(Response response) {
    return SeedsResponse.fromJson(response.data);
  }
}
