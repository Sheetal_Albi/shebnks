import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SignUpRequest.dart';
import 'package:shebnks/model/remote/response/SignUpResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';
import 'package:shebnks/model/remote/util/ApiService.dart';
import 'package:shebnks/model/remote/util/DataFetchCall.dart';

class SignUpCall extends DataFetchCall<SignUpResponse> {
  SignUpRequest _request;

  SignUpCall(SignUpRequest request,
      BehaviorSubject<ApiResponse<SignUpResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.signUpData(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(SignUpResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  SignUpResponse parseJson(Response response) {
    return SignUpResponse.fromJson(response.data);
  }
}
