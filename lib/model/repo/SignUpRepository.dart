import 'package:rxdart/rxdart.dart';
import 'package:shebnks/model/remote/requests/SignUpRequest.dart';
import 'package:shebnks/model/remote/response/SignUpResponse.dart';
import 'package:shebnks/model/remote/util/ApiResponse.dart';

import 'calls/SignUpCall.dart';

class SignUpRepository {
  executeWorkerCategoryData(SignUpRequest signUpRequest,
      BehaviorSubject<ApiResponse<SignUpResponse>> responseSubject) {
    new SignUpCall(signUpRequest, responseSubject).execute();
  }
}
