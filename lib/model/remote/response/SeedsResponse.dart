import 'package:shebnks/model/beans/SeedsData.dart';

import '../../beans/BaseData.dart';

class SeedsResponse extends BaseData {
  SeedsData result;

  SeedsResponse.fromJson(Map<String, dynamic> json)
      : result = SeedsData.fromJson(json),
        super.fromJson(json);
}
