import 'package:shebnks/model/beans/LoginData.dart';

import '../../beans/BaseData.dart';

class LoginResponse extends BaseData {
  LoginData result;

  LoginResponse.fromJson(Map<String, dynamic> json)
      : result = LoginData.fromJson(json),
        super.fromJson(json);
}
