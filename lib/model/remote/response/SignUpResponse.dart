import 'package:shebnks/model/beans/SignUpData.dart';

import '../../beans/BaseData.dart';

class SignUpResponse extends BaseData {
  SignUpData result;

  SignUpResponse.fromJson(Map<String, dynamic> json)
      : result = SignUpData.fromJson(json),
        super.fromJson(json);
}
