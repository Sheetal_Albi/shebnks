class ApiConstants {
  // static final String BASE_URL = "http://13.58.153.70:8081/api/users/";
  static final String BASE_URL = "http://192.168.1.14:5000/api/";

  //api name
  static final String loginApi = "users/login";
  static final String signUpApi = "users/register";
  static final String getSeedsApi = "seedfunds/getseed";

  // parameter
  static final String email = 'email';
  static final String password = 'password';
  static final String tel = 'tel';
  static final String businessName = 'business_name';
  static final String businessAddress = 'business_address';
  static final String businessRegistrationNum = 'business_registration_num';
  static final String businessValuation = 'business_valuation';
  static final String businessEquatyHolder = 'business_equaty_holder';
  static final String country = 'country';
  static final String businessDocument = 'business_document';
}
