import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:shebnks/model/beans/EquityHoldersData.dart';

import '../ApiConstants.dart';

class SignUpRequest {
  String email;
  String password;
  String tel;
  String businessName;
  String businessAddress;
  String businessRegistrationNum;
  String businessValuation;
  List<EquityHolderData> businessEquatyHolder;
  String country;
  List<File> businessDocument;

  SignUpRequest({
    this.email,
    this.password,
    this.tel,
    this.businessName,
    this.businessAddress,
    this.businessRegistrationNum,
    this.businessValuation,
    this.businessEquatyHolder,
    this.country,
    this.businessDocument,
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.email] = email;
    map[ApiConstants.password] = password;
    print("SignUpRequest: " + map.toString());
    return map;
  }

  Future<FormData> toFormData() async {
    var formData = FormData();
    for (var file in businessDocument) {
      formData.files.addAll([
        MapEntry(
            ApiConstants.businessDocument,
            await MultipartFile.fromFile(
              file.path,
              filename: file.uri.toString().split('/').last,
            )),
      ]);
    }
    formData.fields.add(MapEntry(ApiConstants.email, email));
    formData.fields.add(MapEntry(ApiConstants.password, password));
    formData.fields.add(MapEntry(ApiConstants.tel, tel));
    formData.fields.add(MapEntry(ApiConstants.businessName, businessName));
    formData.fields
        .add(MapEntry(ApiConstants.businessAddress, businessAddress));
    formData.fields.add(MapEntry(
        ApiConstants.businessRegistrationNum, businessRegistrationNum));
    formData.fields
        .add(MapEntry(ApiConstants.businessValuation, businessValuation));
    formData.fields.add(MapEntry(
        ApiConstants.businessEquatyHolder, jsonEncode(businessEquatyHolder)));
    formData.fields.add(MapEntry(ApiConstants.country, country));

    print("SignUpRequest: " + formData.toString());
    return formData;
  }
}
