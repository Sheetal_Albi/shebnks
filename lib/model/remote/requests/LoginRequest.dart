import 'dart:core';

import '../ApiConstants.dart';

class LoginRequest {
  String email;
  String password;

  LoginRequest(this.email, this.password);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.email] = email;
    map[ApiConstants.password] = password;
    print("LoginRequest: " + map.toString());
    return map;
  }
}
