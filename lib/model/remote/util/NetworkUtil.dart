import 'package:dio/dio.dart';
import 'package:shebnks/view/constants/Constant.dart';

import '../ApiConstants.dart';

class NetworkUtil {
  Dio _dio;

  NetworkUtil() {
    ///Create Dio Object using baseOptions set receiveTimeout,connectTimeout
    BaseOptions options = BaseOptions();
    options.baseUrl = ApiConstants.BASE_URL;
    _dio = Dio(options);
    _dio.interceptors.add(LogInterceptor());
  }

  ///used for calling Get Request
  Future<Response> get(String url, Map<String, String> params) async {
    Map<String, dynamic> header = new Map();
    header["Authorization"] =
        "Bearer " + await ConstantFunctions.getSF(sfToken);

    Response response = await _dio.get(url,
        // queryParameters: params,
        options: Options(headers: header, responseType: ResponseType.json));
    return response;
  }

  /*///used for calling post Request
  Future<Response> post(String url, Map<String, dynamic> params) async {

    Map<String, dynamic> header = new Map();
    header["Authorization"] = "Bearer " + await ConstantFunctions.getSF(sfToken);

    Response response = await _dio.post(url,
        data: params, options: Options(
            headers: header,
            responseType: ResponseType.json));
    return response;
  }*/

  ///used for calling post Request
  Future<Response> post(String url, Map<String, dynamic> params) async {
    Response response = await _dio.post(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }

  ///used for calling post Request with files as FormData
  Future<Response> postWithFiles(String url, FormData params) async {
    Response response = await _dio.post(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }
}
