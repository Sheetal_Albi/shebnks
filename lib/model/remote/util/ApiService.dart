import 'package:dio/dio.dart';
import 'package:shebnks/model/remote/requests/LoginRequest.dart';
import 'package:shebnks/model/remote/requests/SeedsRequest.dart';
import 'package:shebnks/model/remote/requests/SignUpRequest.dart';

import '../ApiConstants.dart';
import 'NetworkUtil.dart';

/// write your all API Async requests here
class ApiService {
  NetworkUtil networkUtil = NetworkUtil();

  // Login
  Future<Response> loginData(LoginRequest loginRequest) {
    return networkUtil.post(ApiConstants.loginApi, loginRequest.toMap());
  }

  // Sign Up
  Future<Response> signUpData(SignUpRequest signUpRequest) async {
    FormData formData = await signUpRequest.toFormData();
    return networkUtil.postWithFiles(ApiConstants.signUpApi, formData);
  }

  // getSeeds
  Future<Response> getSeedsData(SeedsRequest seedsRequest) {
    return networkUtil.get(ApiConstants.getSeedsApi, null);
  }
}

///Single final Object of API Service
final apiServiceInstance = ApiService();
